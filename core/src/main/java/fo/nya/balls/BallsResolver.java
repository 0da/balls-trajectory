package fo.nya.balls;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Простой класс-утилита для выталкивания шариков друг из друга.
 */
public class BallsResolver {

    /**
     * Временный вектор для промежуточных вычислений.
     */
    private static final Vector2 tmp = new Vector2();

    /**
     * 'Сила' с которой шарики будут вылазить друг из друга.
     */
    private static final float RELAX_FACTOR = 0.01f;

    /**
     * Метод выталкивает шарики друг из друга с определенной {@link #RELAX_FACTOR 'силой'} если расстояние между ними меньше чем {@link TimeFrame#EPSILON}.
     *
     * @param balls список шариков которые надо растолкать.
     */
    public static void relax(Array<Ball> balls) {

        boolean repeat = true;

        while (repeat) {

            repeat = false;

            for (int i = 0; i < balls.size; i++) {
                for (int j = i + 1; j < balls.size; j++) {

                    Ball a = balls.get(i);
                    Ball b = balls.get(j);

                    float d = a.distance(0, b);

                    if (d < -TimeFrame.EPSILON) {
                        tmp.set(a.pos).sub(b.pos).scl(RELAX_FACTOR);

                        // В случае если находятся идеально на одном месте, выталкиваем в случайную сторону.
                        if (tmp.isZero()) {
                            tmp.add(ThreadLocalRandom.current().nextFloat(), ThreadLocalRandom.current().nextFloat());
                        }

                        a.pos.add(tmp);
                        b.pos.sub(tmp);

                        repeat = true;
                    }
                }
            }
        }
    }
}
