package fo.nya.balls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import fo.nya.balls.input.BallPlacementInputAdapter;
import fo.nya.balls.input.DirectionChangeInputAdapter;
import fo.nya.balls.input.DragInputAdapter;
import fo.nya.balls.input.KeyUpCallbackInputAdapter;

/**
 * Основной и единственный экран в этом приложении.
 */
public class MainScreen extends ScreenAdapter {

    /**
     * Количество сегментов для отрисовки кругов, чтоб они небыли шестиугольниками.
     */
    private final static int SEGMENTS_PER_CIRCLE = 30;

    /**
     * Длинна 'бесконечной' траектории.
     */
    private final static float TRAJECTORY_LENGTH = 1000;

    /**
     * Количество контактов которое нужно отрисовать прежде чем закончить 'симуляцию'.
     */
    private final static int CONTACTS_AMOUNT = 8;

    /**
     * ShapeRenderer для отрисовки геометрии.
     */
    private final ShapeRenderer renderer = new ShapeRenderer();

    /**
     * Viewport экрана, чтоб картинка была не 'растянутой'.
     */
    private final Viewport viewport = new ExtendViewport(160, 100);

    /**
     * Координата указателя после {@link Viewport#unproject(Vector2)}.
     */
    private final Vector2 mouse = new Vector2();

    /**
     * Оригинальное, самое первое состояние шаров, именно сюда добавляются новые шары для дальнейшего расчета траекторий.
     */
    private final TimeFrame origin = new TimeFrame(new Array<>(CONTACTS_AMOUNT));

    public MainScreen() {
        Gdx.input.setInputProcessor(new InputMultiplexer(
                new DragInputAdapter(viewport, origin.balls),
                new BallPlacementInputAdapter(viewport, origin.balls),
                new DirectionChangeInputAdapter(viewport, origin.balls),
                new KeyUpCallbackInputAdapter(Input.Keys.R, origin.balls::clear)
        ));
    }

    @Override public void render(float delta) {

        // Чистим экран.
        ScreenUtils.clear(Color.BLACK);

        // Обновляем позицию мыши в координатах 'мира'.
        viewport.unproject(mouse.set(Gdx.input.getX(), Gdx.input.getY()));

        // Обновляем 'камеру' и начинаем рендеринг.
        renderer.setProjectionMatrix(viewport.getCamera().combined);
        renderer.begin(ShapeRenderer.ShapeType.Line);

        // Рисуем круг возле указателя мыши.
        renderer.setColor(Color.DARK_GRAY);
        renderer.circle(mouse.x, mouse.y, BallPlacementInputAdapter.DEFAULT_RADIUS, SEGMENTS_PER_CIRCLE);

        TimeFrame prev = origin;

        for (int i = 0; i < CONTACTS_AMOUNT; i++) {
            TimeFrame next = prev.next();

            drawTimeFrame(next, 0.5f, false, false);

            renderer.setColor(Color.GREEN);

            for (int j = 0; j < next.balls.size; j++) {
                Ball a = next.balls.get(j);
                Ball b = prev.balls.get(j);

                renderer.line(a.pos.x, a.pos.y, b.pos.x, b.pos.y);
            }

            prev = next;
        }

        drawTimeFrame(prev, 0.5f, false, true);
        drawTimeFrame(origin, 1, true, false);

        renderer.end();
    }

    /**
     * Отрисовать один 'промежуток' симуляции.
     *
     * @param frame              промежуток который нужно отрисовать.
     * @param saturation         'Яркость' цветов.
     * @param all                флаг того что нужно отрисовать все шарики, иначе(если {@code false}) будут отрисованы только шары у которых был контакт({@link TimeFrame#contacted}).
     * @param infiniteTrajectory флаг того, что нужно нарисовать 'бесконечную' траекторию.
     */
    public void drawTimeFrame(TimeFrame frame, float saturation, boolean all, boolean infiniteTrajectory) {
        for (Ball ball : frame.balls) {

            Vector2 pos = ball.pos;
            Vector2 dir = ball.dir;

            if (all || frame.contacted.contains(ball, true)) {
                Color color = ball.color;

                // ShapeRenderer не дружит с альфой из коробки, простой костыль.
                renderer.setColor(color.r * saturation, color.g * saturation, color.b * saturation, color.a);
                renderer.circle(pos.x, pos.y, ball.radius, SEGMENTS_PER_CIRCLE);
            }

            if (infiniteTrajectory) {
                renderer.setColor(Color.GREEN);
                renderer.line(pos.x, pos.y, pos.x + dir.x * TRAJECTORY_LENGTH, pos.y + dir.y * TRAJECTORY_LENGTH);
            }
        }
    }

    @Override public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override public void dispose() {
        renderer.dispose();
    }
}
