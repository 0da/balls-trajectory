package fo.nya.balls;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Итеративный поиск коллизий между шариками.
 *
 * @implNote После создания объекта, изменение состояния {@link #balls нариков} не предусмотренно.
 * @see #next()
 */
public class TimeFrame {

    /**
     * Константа для уменьшения значения {@code step} в методе {@link #next()}.
     * <br> В данном случае взято число 'золотого сечения'('phi', 'ф') равное {@code (1 + sqrt(5)) / 2}.
     * <br> Также число 'инвертировано'({@code 1/ф}), чтоб заменить деление на умножение в симуляции.
     */
    private final static float CONST = (float) (1 / ((1 + Math.sqrt(5)) / 2));

    /**
     * Изначальное значение шага в методе {@link #next()}.
     */
    private final static float INITIAL_STEP = 1;

    /**
     * Максимально значение {@code t} в методе {@link #next()} до которого происходит симуляция, нужно чтоб не уйти в бесконечный цикл.
     */
    private final static float MAXIMUM_T = 50;

    /**
     * Минимальное значение {@code t} в методе {@link #next()} до которого происходит симуляция, нужно чтоб не уйти в бесконечный цикл.
     */
    private final static float MINIMUM_T = 0.0001f;

    /**
     * Расстояние между шариками при котором их состояние считается за 'контакт'.
     */
    public final static float EPSILON = 0.0001f;

    /**
     * Временный вектор для промежуточных вычислений.
     */
    private final Vector2 tmp = new Vector2();

    /**
     * Список всех контактирующих шариков. Данный лист обновляется только в конструкторе при создании объекта. Последующее изменение состояние шариков не будет учитываться.
     */
    public final Array<Ball> contacted = new Array<>();

    /**
     * Список всех шариков.
     */
    public final Array<Ball> balls;

    /**
     * Конструктор создает новый объект типа {@link TimeFrame}.
     * В конструкторе обновляется список {@link #contacted} с помощью метода {@link #resolveContacts()}.
     *
     * @param balls Список всех шариков.
     * @see #contacted
     * @see #balls
     */
    public TimeFrame(Array<Ball> balls) {
        this.balls = balls;
        resolveContacts();
    }

    /**
     * Метод находит все касающеюся шарики, и обновляет их направление движения.
     * <br> Шарики считаются касающимися если расстояние между ними меньше чем {@link #EPSILON}.
     */
    private void resolveContacts() {
        for (int i = 0; i < balls.size; i++) {
            for (int j = i + 1; j < balls.size; j++) {

                Ball a = balls.get(i);
                Ball b = balls.get(j);

                float d = a.distance(0, b);

                if (Math.abs(d) <= EPSILON) {
                    tmp.set(a.pos).sub(b.pos).nor();

                    if (tmp.isZero()) {
                        System.out.println(1);
                    }

                    float aci = a.dir.dot(tmp);
                    float bci = b.dir.dot(tmp);

                    tmp.scl(bci - aci);

                    a.dir.add(tmp).nor();
                    b.dir.sub(tmp).nor();


                    // Следующие два условия нужны на случай если новое направление будет полностью противоположено старому.
                    // Без этих условий шарик просто остановится на месте, что возможно было бы валидно в симуляции с массой,
                    // но в случае отображения траектории, мне кажется так будет лучше.
                    if (a.dir.isZero()) {
                        a.dir.add(tmp).nor();
                    }

                    if (b.dir.isZero()) {
                        b.dir.sub(tmp).nor();
                    }

                    contacted.add(a);
                    contacted.add(b);
                }
            }
        }
    }

    /**
     * Метод вычисляющий следующий шаг симуляции.
     * <br>Псевдокод алгоритма:
     * <pre>
     * {@code
     *
     *  final float const = //...
     *  float step = //...
     *  float t = 0;
     *
     *  while(true) {
     *      t += step;
     *
     *      switch (state(t)) {
     *          case "too much" -> {
     *              t -= step;
     *              step /= const
     *          }
     *
     *          case "contact" -> {
     *              return t;
     *          }
     *      }
     *  }
     * }
     * </pre>
     * Пояснение: Метод проверяет состояние шариков через {@code t} времени симуляции.
     * В случае если коллизии не случилось, то метод прибавляет к {@code t} переменную {@code step} и начинает проверку сначала.
     * Если коллизия случилась, то возможно два варианта. Первый вариант: Коллизия слишком большая,
     * один шарик наслаивается на другой слишком сильно, в этом случае значение {@code t} возвращается в свое предыдущее значение,
     * а переменная {@code step} уменьшается в {@code const} раз, симуляция повторяется сначала.
     * Второй вариант: Расстояние между шариками настолько маленькое, что состояние можно считать за 'контакт'. В этом случае симуляция завершается.
     *
     * @return новое состояние, в новом объекте {@link TimeFrame}.
     */
    public TimeFrame next() {
        float t = 0;
        float step = INITIAL_STEP;

        boolean contact;

        main:
        while (true) {

            t += step;
            contact = false;

            if (t < MINIMUM_T) {
                t = MINIMUM_T;
                break;
            }

            for (int i = 0; i < balls.size; i++) {
                for (int j = i + 1; j < balls.size; j++) {

                    Ball a = balls.get(i);
                    Ball b = balls.get(j);

                    float d = a.distance(t, b);

                    if (Math.abs(d) < EPSILON) {
                        contact = true;
                    } else if (d < 0) {
                        t -= step;
                        step *= CONST;

                        continue main;
                    }
                }
            }

            if (contact || t > MAXIMUM_T) {
                break;
            }
        }

        Array<Ball> next = new Array<>(balls.size);

        for (Ball ball : balls) {
            next.add(ball.step(t));
        }

        return new TimeFrame(next);
    }
}
