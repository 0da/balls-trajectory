package fo.nya.balls.input;

import com.badlogic.gdx.InputAdapter;

/**
 * InputAdapter для совершения действия при нажатии на кнопку.
 */
public class KeyUpCallbackInputAdapter extends InputAdapter {

    /**
     * 'Key Code' кнопки при нажатии которой будет вызвано действие {@link #runnable}.
     */
    private final int keyCode;

    /**
     * Действие, которое будет вызвано при нажатии на кнопку {@link #keyCode}
     */
    private final Runnable runnable;

    public KeyUpCallbackInputAdapter(int keyCode, Runnable runnable) {
        this.keyCode = keyCode;
        this.runnable = runnable;
    }

    @Override public boolean keyUp(int keycode) {
        if (keycode == keyCode) {
            runnable.run();
            return true;
        }
        return false;
    }
}
