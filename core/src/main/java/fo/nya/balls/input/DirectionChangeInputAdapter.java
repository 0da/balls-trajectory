package fo.nya.balls.input;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import fo.nya.balls.Ball;

/**
 * InputAdapter для изменения направления движения шарика.
 */
public class DirectionChangeInputAdapter extends InputAdapter {

    /**
     * {@link Viewport} с помощью которого рисуется экран.
     */
    private final Viewport viewport;

    /**
     * Кеш для {@link Viewport#unproject(Vector2)}
     */
    private final Vector2 u = new Vector2();

    /**
     * Ссылка на список со всеми шариками.
     */
    private final Array<Ball> balls;

    public DirectionChangeInputAdapter(Viewport viewport, Array<Ball> balls) {
        this.viewport = viewport;
        this.balls = balls;
    }

    @Override public boolean mouseMoved(int screenX, int screenY) {
        viewport.unproject(u.set(screenX, screenY));
        return false;
    }

    @Override public boolean scrolled(float amountX, float amountY) {
        for (Ball ball : balls) {
            if (ball.isInside(u.x, u.y)) {
                ball.dir.rotateDeg(amountX + amountY).nor();
                return true;
            }
        }

        return false;
    }
}
