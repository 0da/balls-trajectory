package fo.nya.balls.input;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import fo.nya.balls.Ball;
import fo.nya.balls.BallsResolver;

/**
 * InputAdapter для перетаскивания шариков.
 */
public class DragInputAdapter extends InputAdapter {

    /**
     * {@link Viewport} с помощью которого рисуется экран.
     */
    private final Viewport viewport;

    /**
     * Кеш для {@link Viewport#unproject(Vector2)}
     */
    private final Vector2 u = new Vector2();

    /**
     * Ссылка на список со всеми шариками.
     */
    private final Array<Ball> balls;

    /**
     * Текущий захваченный шарик.
     */
    private Ball target;

    public DragInputAdapter(Viewport viewport, Array<Ball> balls) {
        this.viewport = viewport;
        this.balls = balls;
    }

    @Override public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        viewport.unproject(u.set(screenX, screenY));

        for (Ball ball : balls) {
            if (ball.isInside(u.x, u.y)) {
                target = ball;
                return true;
            }
        }

        return false;
    }

    @Override public boolean touchDragged(int screenX, int screenY, int pointer) {

        if (target != null) {
            viewport.unproject(u.set(screenX, screenY));
            target.pos.set(u);
            BallsResolver.relax(balls);
            return true;
        }

        return false;
    }

    @Override public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if (target != null) {
            target = null;
            return true;
        }

        return false;
    }
}
