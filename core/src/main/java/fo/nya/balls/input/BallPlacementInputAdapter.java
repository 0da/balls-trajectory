package fo.nya.balls.input;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;
import fo.nya.balls.Ball;
import fo.nya.balls.BallsResolver;

/**
 * InputAdapter для расстановки новых шариков.
 */
public class BallPlacementInputAdapter extends InputAdapter {

    /**
     * Радиус шарика.
     */
    public final static float DEFAULT_RADIUS = 3;

    /**
     * {@link Viewport} с помощью которого рисуется экран.
     */
    private final Viewport viewport;

    /**
     * Кеш для {@link Viewport#unproject(Vector2)}
     */
    private final Vector2 u = new Vector2();

    /**
     * Ссылка на список со всеми шариками.
     */
    private final Array<Ball> balls;

    public BallPlacementInputAdapter(Viewport viewport, Array<Ball> balls) {
        this.viewport = viewport;
        this.balls = balls;
    }

    @Override public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        viewport.unproject(u.set(screenX, screenY));

        if (balls.isEmpty()) {
            balls.add(new Ball(u.x, u.y, DEFAULT_RADIUS, 0, 1f, Color.RED));
        } else {
            balls.add(new Ball(u.x, u.y, DEFAULT_RADIUS, 0, 0, Color.WHITE));
            BallsResolver.relax(balls);
        }

        return true;
    }
}
