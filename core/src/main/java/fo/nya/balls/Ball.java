package fo.nya.balls;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

/**
 * Объект представляющий собой один шарик.
 */
public final class Ball {
    /**
     * Текущая позиция.
     */
    public final Vector2 pos;

    /**
     * Радиус.
     */
    public final float radius;

    /**
     * Направление движения.
     */
    public final Vector2 dir;

    /**
     * Цвет.
     */
    public final Color color;

    /**
     * Конструктор для создания объекта.
     *
     * @param x      текущая позиция по оси {@code X}.
     * @param y      текущая позиция по оси {@code Y}.
     * @param radius радиус.
     * @param dx     направление движения по оси {@code X}.
     * @param dy     направление движения по оси {@code Y}.
     * @param color  цвет.
     * @implNote конструктор нормализует вектор направления движения.
     */
    public Ball(float x, float y, float radius, float dx, float dy, Color color) {
        this.pos = new Vector2(x, y);
        this.radius = radius;
        this.dir = new Vector2(dx, dy).nor();
        this.color = color;
    }

    /**
     * Дистанция между этим и {@code other} шариками спустя {@code delta} времени.
     * Дистанция считается 'от стенки до стенки', а не 'от центра до центра'.
     *
     * @param delta время через которое нужно узнать дистанцию между шариками.
     * @param other шар дистанцию до которого нужно рассчитать.
     * @return Дистанция между этим и {@code other} шариками спустя {@code delta} времени. Дистанция может быть отрицательной, если один шар находится внутри другого.
     */
    public float distance(float delta, Ball other) {
        final float dx = (other.pos.x + other.dir.x * delta) - (pos.x + dir.x * delta);
        final float dy = (other.pos.y + other.dir.y * delta) - (pos.y + dir.y * delta);
        final float r = radius + other.radius;

        return dx * dx + dy * dy - r * r;
    }

    /**
     * Метод проверяет нахождение точки внутри шара.
     *
     * @param x координата {@code X} для точки.
     * @param y координата {@code Y} для точки.
     * @return {@code true} если точка находится внутри шара, иначе {@code false}.
     */
    public boolean isInside(float x, float y) {
        final float dx = x - pos.x;
        final float dy = y - pos.y;
        return (dx * dx + dy * dy - radius * radius) < 0;

    }

    /**
     * Метод создает копию этого шара с учетом движения в направлении {@link #dir} в течение {@code delta} времени.
     * <pre>{@code
     *  return new Ball( ..., pos + dir * delta, ...);
     * }</pre>
     *
     * @param delta время движения.
     * @return новый объект с измененной позицией.
     * @implNote Все внутренние объекты также копируются и не связаны с предыдущим шаром.
     */
    public Ball step(float delta) {
        return new Ball(pos.x + dir.x * delta, pos.y + dir.y * delta, radius, dir.x, dir.y, color);
    }
}
